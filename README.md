# football-assistant

Description
    Project is done in express.js framework (backend - rest api) and angular2 framework(frontend). We were trying to use some design patterns for clearer arhitecture.

Functionalities

    - organization of tournaments
    - registering/login of teams and users
    - invitations to players on email
    - adding playground
    - creating turnament on choosen playground
    - list of teams for every tournament
    - application for tournaments of teams is done by the capitan
    
    
Design patterns used in this project

    Domain model
        - project has models of: tournament, team, user, playground
        - for every model, there is a controller and belonging BLL
        - controllers are here for data validations and to call BLL
        - BLL is logic where we are getting and transforming data
        - data layer is done with Data Mapper in MongoJS library
    
    Service layer
        - Backend is REST API written in express.js framework (JS)
    
    MVC and Template view
        - for frontend we are using Angular2 which is MVC oriented and is using Template View
    
    Identity field
        - saving all model ids in local storage or in url to hold the connection between the object and database
    
    Database session state
        - when the user is logging in, we generate token on serverside and we save it in DB, together with datetime now() and we return that token back to client.
        - when user wants to call API services for which it has to have specified role, or just logged in, we also send that token in head of request too
        - then on the server we check if there are permissions for that user for that service