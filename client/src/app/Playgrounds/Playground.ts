export class Playground {
    constructor(public _id: string, public name: string, public address: string, locationX?: string, locationY?: string) { }
}