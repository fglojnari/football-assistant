import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs';
import { Subject } from 'rxjs/Subject';

import { Playground } from './Playground';

@Injectable()
export class PlaygroundService {
  private baseUrl: string = "http://127.0.0.1:3000";

  constructor(private http: Http) { }

  add(playground: Playground): Observable<string> {
    let user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      let headers = new Headers({ 'Content-Type': 'application/json', 'token': user.token });
      return this.http.post(this.baseUrl + "/playgrounds", JSON.stringify(playground), { headers: headers })
        .map((response: Response) => {
          return response.json();
        })
        .catch((error: Response): any => {
          return Observable.throw(error.json());
        });
    } else {
      return Observable.throw("Permission denied!");
    }
  }

  getPlaygrounds(): Observable<Playground[]> {
    let user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      let headers = new Headers({ 'Content-Type': 'application/json', 'token': user.token });
      return this.http.get(this.baseUrl + "/playgrounds", { headers: headers })
        .map((response: Response) => {
          return response.json();
        })
        .catch((error: Response): any => {
          return Observable.throw(error.json());
        });
    } else {
      return Observable.throw("Permission denied!");
    }
  }

}
