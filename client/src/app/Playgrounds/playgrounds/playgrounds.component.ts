import { ActivatedRoute, Router, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Playground } from '../Playground';
import { NgForm } from '@angular/forms';

import { PlaygroundService } from '../playground.service';

@Component({
  selector: 'app-playgrounds',
  templateUrl: './playgrounds.component.html',
  styleUrls: ['./playgrounds.component.css']
})
export class PlaygroundsComponent implements OnInit {
  playground: Playground;
  errorMessage: string;
  playgrounds: Playground[];

  constructor(private playgroundService: PlaygroundService) { }

  ngOnInit() {
    this.playground = new Playground("", "", "", "", "");

    this.playgroundService.getPlaygrounds().subscribe(playgrounds => {
      this.playgrounds = playgrounds;
    }, error => { this.errorMessage = error; });
  }

  addPlayground(valid: boolean, form: NgForm) {
    this.playgroundService.add(this.playground).subscribe(message => {
      this.errorMessage = message;
    }, error => { this.errorMessage = error; });
    this.playgrounds.push(this.playground);
  }

  resetForm(form:NgForm) {
    this.errorMessage = "";
    form.resetForm();
  }

}
