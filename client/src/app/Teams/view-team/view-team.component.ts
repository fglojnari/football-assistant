import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { NgForm } from '@angular/forms';

import { Team } from "../Team";
import { TeamMember } from "../TeamMember";

import { TeamsService } from "../teams.service";

@Component({
  selector: 'app-view-team',
  templateUrl: './view-team.component.html',
  styleUrls: ['./view-team.component.css']
})
export class ViewTeamComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute,private teamService: TeamsService) { }
  team: Team;
  teamMembers: TeamMember[];
  userName: string;
  invitedMemberEmail: string;
  errorMessage: string = "";

  ngOnInit() {
    //Local loading
    this.userName = JSON.parse(localStorage.getItem("user")).username;
    let tempTeam = this.teamService.getTeamLocal();
    if (tempTeam) {
      this.team = tempTeam;
      this.getMembers();
    }
    else //Reading product from server database.
    {
      this.route.params.subscribe((params: Params) => {
        let id = params["id"];
        this.teamService.getTeam(id).subscribe(
          team => {
            this.team = team;
            this.getMembers();
          },
          error => {
            console.log(error);
          });
      });
    }
  }

  submit(valid: boolean, form: NgForm) {
      this.teamService.inviteMember(this.invitedMemberEmail, this.team._id).subscribe(isInvited => {
        this.resetForm(form);
        this.errorMessage = "Member invited";
      }, error => { this.errorMessage = error; });
  }

  resetForm(form:NgForm) {
    this.errorMessage = "";
    form.resetForm();
  }

  getMembers() {
      this.teamService.getMembers(this.team._id).subscribe(members => {
        this.teamMembers = members;
      }, error => { this.errorMessage = error; });
  }

}
