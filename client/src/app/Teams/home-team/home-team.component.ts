import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

//Classes
import { Team } from "../Team";

//Services
import { TeamsService } from "../teams.service";

@Component({
  selector: 'app-home-team',
  templateUrl: './home-team.component.html',
  styleUrls: ['./home-team.component.css']
})
export class HomeTeamComponent implements OnInit {

  constructor(private router: Router, private teamService: TeamsService) { }
  teams: Team[];

  ngOnInit() {
    this.teamService.getTeams().subscribe(
      teams => {
        this.teams = teams; //Database teams that loads once.
      },
      error => {
        this.teams = [];
        console.log(error);
      });

  }
  saveTempTeam(team: Team) {
    this.teamService.setTeamLocal(team);
    this.router.navigate(['/view', team._id]);
  }

}
