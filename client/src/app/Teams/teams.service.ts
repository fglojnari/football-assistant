import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs';
import { Subject } from 'rxjs/Subject';

import { Team } from './Team';
import { TeamMember } from "./TeamMember";

@Injectable()
export class TeamsService {
  private baseUrl: string = "http://127.0.0.1:3000";
  tempTeam: Team = null;

  constructor(private http: Http) { }

  add(team: Team): Observable<Team> {
    let user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      let headers = new Headers({ 'Content-Type': 'application/json', 'token': user.token });
      return this.http.post(this.baseUrl + "/teams", JSON.stringify(team), { headers: headers })
        .map((response: Response) => {
          return response.json();
        })
        .catch((error: Response): any => {
          return Observable.throw(error.json());
        });
    } else {
      return Observable.throw("Permission denied!");
    }
  }

  getTeams(): Observable<Team[]> {
    let user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      let headers = new Headers({ 'Content-Type': 'application/json', 'token': user.token });
      return this.http.get(this.baseUrl + "/teams", { headers: headers })
        .map((response: Response) => {
          return response.json();
        })
        .catch((error: Response): any => {
          return Observable.throw(error.json());
        });
    } else {
      return Observable.throw("Permission denied!");
    }
  }

  getTeam(id: string): Observable<Team> {    
    let user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      let headers = new Headers({ 'Content-Type': 'application/json', 'token': user.token });
      return this.http.get(this.baseUrl + "/teams/" + id, { headers: headers })
        .map((response: Response) => {
          return response.json();
        })
        .catch((error: Response): any => {
          return Observable.throw(error.json());
        });
    } else {
      return Observable.throw("Permission denied!");
    }
  }

  setTeamLocal(team: Team) {
    this.tempTeam = team;
  }

  getTeamLocal() {
    return this.tempTeam;
  }

  inviteMember(email: string, team_id: string) {
    let user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      let headers = new Headers({ 'Content-Type': 'application/json', 'token': user.token });
      return this.http.post(this.baseUrl + "/teams/invite", { email: email, team_id: team_id }, { headers: headers })
        .map((response: Response) => {
          return response.json();
        })
        .catch((error: Response): any => {
          return Observable.throw(error.json());
        });
    } else {
      return Observable.throw("Permission denied!");
    }
  }

  getMembers(team_id: string): Observable<TeamMember[]> {
    let user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      let headers = new Headers({ 'Content-Type': 'application/json', 'token': user.token });
      return this.http.get(this.baseUrl + "/teams/members/" + team_id, { headers: headers })
        .map((response: Response) => {
          return response.json();
        })
        .catch((error: Response): any => {
          return Observable.throw(error.json());
        });
    } else {
      return Observable.throw("Permission denied!");
    }
  }

  getTeamsOfLoggedUser(): Observable<Team[]> {
    let user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      let headers = new Headers({ 'Content-Type': 'application/json', 'token': user.token });
      return this.http.get(this.baseUrl + "/teams/of/user", { headers: headers })
        .map((response: Response) => {
          return response.json();
        })
        .catch((error: Response): any => {
          return Observable.throw(error.json());
        });
    } else {
      return Observable.throw("Permission denied!");
    }
  }

  getTeamsOfTournament(tournamentId): Observable<Team[]> {
    let user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      let headers = new Headers({ 'Content-Type': 'application/json', 'token': user.token });
      return this.http.get(this.baseUrl + "/teams/tournament/" + tournamentId, { headers: headers })
        .map((response: Response) => {
          return response.json();
        })
        .catch((error: Response): any => {
          return Observable.throw(error.json());
        });
    } else {
      return Observable.throw("Permission denied!");
    }
  }
  addTeamToTournament(team_id, tournament_id): Observable<string> {
    let user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      let headers = new Headers({ 'Content-Type': 'application/json', 'token': user.token });
      return this.http.post(this.baseUrl + "/teams/tournament/", {team_id: team_id, tournament_id: tournament_id }, { headers: headers })
        .map((response: Response) => {
          return response.json();
        })
        .catch((error: Response): any => {
          return Observable.throw(error.json());
        });
    } else {
      return Observable.throw("Permission denied!");
    }
  }
}
