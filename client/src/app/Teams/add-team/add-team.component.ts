import { ActivatedRoute, Router, Params } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DropdownModule } from "ng2-dropdown";
import { NgForm } from '@angular/forms';

//Services
import { TeamsService } from "../teams.service";

//Classes
import { Team } from '../Team';

@Component({
  selector: 'app-add-team',
  templateUrl: './add-team.component.html',
  styleUrls: ['./add-team.component.css']
})
export class AddTeamComponent implements OnInit {

  team: Team;
  errorMessage: string = "";

  constructor(private teamService: TeamsService) { }

  ngOnInit() {
    this.team = new Team("", "", "");
  }

  addTeam(valid, form: NgForm, event) {
    event.preventDefault();
    let user = JSON.parse(localStorage.getItem("user"));
    this.team.capitanId = user.id;
    //send to server
    this.teamService.add(this.team).subscribe(team => {
      this.resetForm(form);
      this.errorMessage = "Team added";
    }, error => { this.errorMessage = error; });
  }

  resetForm(form:NgForm) {
    this.errorMessage = "";
    form.resetForm();
  }

}
