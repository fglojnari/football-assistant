export class TeamMember {

    constructor(public first_name: string, public last_name: string, public email:string, public status: string) { }
}