import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs';
import { Subject } from 'rxjs/Subject';

import { Tournament } from './Tournament';

@Injectable()
export class TournamentService {
  private baseUrl: string = "http://127.0.0.1:3000";

  constructor(private http: Http) { }

    add(tournament: Tournament): Observable<Tournament> {
    let user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      let headers = new Headers({ 'Content-Type': 'application/json', 'token': user.token });
      return this.http.post(this.baseUrl + "/tournaments", JSON.stringify(tournament), { headers: headers })
        .map((response: Response) => {
          return response.json();
        })
        .catch((error: Response): any => {
          return Observable.throw(error.json());
        });
    } else {
      return Observable.throw("Permission denied!");
    }
  }

  getTournaments(): Observable<Tournament[]> {
    let user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      let headers = new Headers({ 'Content-Type': 'application/json', 'token': user.token });
      return this.http.get(this.baseUrl + "/tournaments", { headers: headers })
        .map((response: Response) => {
          return response.json();
        })
        .catch((error: Response): any => {
          return Observable.throw(error.json());
        });
    } else {
      return Observable.throw("Permission denied!");
    }
  }

  getTournamentById(tournamentId): Observable<Tournament> {
    let user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      let headers = new Headers({ 'Content-Type': 'application/json', 'token': user.token });
      return this.http.get(this.baseUrl + "/tournaments/" + tournamentId, { headers: headers })
        .map((response: Response) => {
          return response.json();
        })
        .catch((error: Response): any => {
          return Observable.throw(error.json());
        });
    } else {
      return Observable.throw("Permission denied!");
    }
  }

}
