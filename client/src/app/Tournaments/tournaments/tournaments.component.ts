import { ActivatedRoute, Router, Params } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DropdownModule } from "ng2-dropdown";
import { NgForm } from '@angular/forms';

import { TournamentService } from '../tournament.service';
import { PlaygroundService } from '../../Playgrounds/playground.service';
import { Tournament } from '../Tournament';
import { Playground } from '../../Playgrounds/Playground';

@Component({
  selector: 'app-tournaments',
  templateUrl: './tournaments.component.html',
  styleUrls: ['./tournaments.component.css']
})
export class TournamentsComponent implements OnInit {
  tournament: Tournament;
  playgrounds: Playground[];
  tournaments: Tournament[];
  errorMessage: string;

  constructor(private tournamentService: TournamentService, private playgroundService: PlaygroundService, private router: Router) { }

  ngOnInit() {
    this.tournament = new Tournament("", null, null, null);
    this.tournaments = [];

    this.playgroundService.getPlaygrounds().subscribe(playgrounds => {
      this.playgrounds = playgrounds;
    }, error => { this.errorMessage = error; });

    this.tournamentService.getTournaments().subscribe(tournaments => {
      this.tournaments = tournaments;
    }, error => { this.errorMessage = error; });    
  }

  addTournament(valid: boolean, form: NgForm, event) {
    event.preventDefault();
    let user = JSON.parse(localStorage.getItem("user"));
    this.tournaments.push(this.tournament);
    this.tournamentService.add(this.tournament).subscribe(tournament => {
      this.resetForm(form);
      this.errorMessage = "Tournament added";
    }, error => { this.errorMessage = error; });
  }

  onChangePlayground(playgroundId) {
    this.tournament.playground = this.playgrounds.find((singlePlayground: Playground) => {
      return singlePlayground._id == playgroundId
    });
  }

  resetForm(form:NgForm) {
    this.errorMessage = "";
    form.resetForm();
  }

  saveTournament(tournament: Tournament) {
    this.router.navigate(['/tournaments', tournament._id]);
  }
}
