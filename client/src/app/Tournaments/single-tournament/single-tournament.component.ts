import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

import { TournamentService } from '../tournament.service';
import { Tournament } from '../Tournament';
import { Playground } from '../../Playgrounds/Playground';
import { Team } from '../../Teams/Team';
import { TeamsService } from '../../Teams/teams.service';

@Component({
  selector: 'app-single-tournament',
  templateUrl: './single-tournament.component.html',
  styleUrls: ['./single-tournament.component.css']
})
export class SingleTournamentComponent implements OnInit {
  tournament: Tournament;
  teamsOfLoggedUser: Team[];
  selectedTeamId: string;
  message: string;
  teamsOfTournament: Team[];
  constructor(private router: Router, private route: ActivatedRoute, private tournamentService: TournamentService, private teamService: TeamsService) { }

  ngOnInit() {
    this.tournament = new Tournament("", null, null, null);
    this.tournament.playground = new Playground("", "", "");
    this.teamsOfLoggedUser = [];
    this.teamsOfTournament = [];
    this.route.params.subscribe((params: Params) => {
        let id = params["id"];
        this.tournamentService.getTournamentById(id).subscribe(
          tournament => {
            this.tournament = tournament;
          },
          error => {
            console.log(error);
        });
        this.teamService.getTeamsOfTournament(id).subscribe(
          teams => {
            this.teamsOfTournament = teams;
          },
          error => {
            this.message = error;
          }
        );
    });

    this.teamService.getTeamsOfLoggedUser().subscribe(
      teams => {
        this.teamsOfLoggedUser = teams;
      },
      error => {
        console.log(error)
      }
    )
  }

  onChangeTeam(team_id: string) {
    this.selectedTeamId = team_id;
  }

  addTeam() {
    this.route.params.subscribe((params: Params) => {
        let tournament_id = params["id"];
        this.teamService.addTeamToTournament(this.selectedTeamId, tournament_id).subscribe(
          message => {
            this.message = message;
          },
          error => {
            console.log(error);
        });
    });
    this.teamsOfTournament.push(this.teamsOfLoggedUser.find(team => {
      return team._id == this.selectedTeamId;
    }));
  }

}
