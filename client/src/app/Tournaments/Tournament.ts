import { Playground } from '../Playgrounds/Playground';

export class Tournament {

    constructor(public name: string, public date_time: Date, public playground: Playground, public _id?: string) { }
}