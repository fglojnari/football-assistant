import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

//Classes
import { User } from '../../Users/User';

//Services
import { AuthenticationService } from '../authentication.service';


@Component({
  moduleId: module.id,
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  
  constructor(private authenticationService: AuthenticationService) { }

  user: User;
  message: string;

  ngOnInit() {
    this.user = new User("", "", "", "", "");
  }

  submit(valid: boolean, formValue: NgForm) {
    if (valid && (formValue.form.value.password == formValue.form.value.confirmPassword)) { 
      let data = formValue.form.value;
      let user = new User(data.first_name, data.last_name, data.username, data.password, data.email);
      this.authenticationService.register(user).subscribe(msg => {
        this.message = msg;
      }, error => {this.message = error});
    }
  }

}
