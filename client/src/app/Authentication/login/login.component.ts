import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

//Classes
import { LoginData } from './LoginData';

//Services
import { AuthenticationService } from '../authentication.service';

@Component({
  moduleId: module.id,
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  loginData: LoginData;
  errorMsg: string;

  ngOnInit() {
    this.loginData = new LoginData("", "");
    this.errorMsg = "";
  }

  login(valid: boolean, formValue: NgForm) {
    this.errorMsg = "";
    if (valid) {
      let loginData = new LoginData(formValue.form.value.username, formValue.form.value.password);
      this.authenticationService.login(loginData).subscribe(
        user => { //OnLogin
          this.authenticationService.updateUserLogin(user);
          this.router.navigate(["/home"]);
        },
        error => {
          this.errorMsg = error;
          this.loginData.password = "";
        });
    }
  }

}
