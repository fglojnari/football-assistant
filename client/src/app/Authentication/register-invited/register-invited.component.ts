import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { NgForm } from '@angular/forms';
//Classes
import { User } from '../../Users/User';

//Services
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-register-invited',
  templateUrl: './register-invited.component.html',
  styleUrls: ['./register-invited.component.css']
})
export class RegisterInvitedComponent implements OnInit {
  user: User;
  isExistingUser: boolean;
  emailOfExisting: string;
  message: string;
  constructor(private authenticationService: AuthenticationService, private route: ActivatedRoute, private router:Router) { }

  ngOnInit() {
    this.user = new User(null, null, null, null, null)
    this.route.params.subscribe((params: Params) => {
      let token = params["token"];
      this.authenticationService.getInvitedUser(token).subscribe(user => {
        if (user.first_name) {
          this.isExistingUser = true;
          this.emailOfExisting = user.email;
        } else {
          this.user = new User(null, null, null, null, user.email);
        }
      }, error => {this.message = error});
    });
  }

  submit(valid: boolean, form: NgForm) {
        this.authenticationService.registerInvitedUser(this.user).subscribe(msg => {
          this.message = msg;
      }, error => {this.message = error});
  }

  submitExisting() {
      this.authenticationService.finalizeInvitationForExistingUser(this.emailOfExisting).subscribe(msg => {
          this.message = msg;
      }, error => {this.message = error});
  }

}
