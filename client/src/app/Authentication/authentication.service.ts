import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

//ReactiveJS
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs';

//Classes
import { User } from '../Users/User';
import { LoginData } from './login/LoginData';

@Injectable()
export class AuthenticationService {
  private baseUrl: string = "http://127.0.0.1:3000";

  constructor(private http: Http) { }

  register(user: User): Observable<string> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post(this.baseUrl + "/authenticate/registration", JSON.stringify(user), { headers: headers })
      .map((response: Response) => {
        return response.json();
      })
      .catch((error: Response): any => {
        return Observable.throw(error.json());
      });
  }

  login(loginData: LoginData): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.post(this.baseUrl + "/authenticate/login", JSON.stringify(loginData), { headers: headers })
      .map((response: Response) => {
        localStorage.setItem("user", JSON.stringify(response.json())); //Sets logged user
        return response.json();
      })
      .catch((error: Response): any => {
        return Observable.throw(error.json());
      });
  }


  activate(activationCode): Observable<string> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.get(this.baseUrl + "/authenticate/token/" + activationCode, { headers: headers })
      .map((response: Response) => {
        return response.json();
      })
      .catch((error: Response): any => {
        return Observable.throw(error.json());
      });
  }

  //Checks if user is logged in
  authenticate(): Observable<any> {
    let user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      let headers = new Headers({ 'Content-Type': 'application/json', 'token': user.token, 'username':user.username });
      return this.http.get(this.baseUrl + "/authenticate/loggedin", { headers: headers })
        .map((response: Response) => {
          return response.json();
        })
        .catch((error: Response): any => {
          this.removeLoggedUser();
          return Observable.throw(error.json());
        });
    }
    else
      return Observable.throw("User not logged in.");
  }

  logout(): Observable<any> {
    let user = JSON.parse(localStorage.getItem("user"));
    if (user) {
      let headers = new Headers({ 'Content-Type': 'application/json', 'token': user.token });
      return this.http.post(this.baseUrl + "/authenticate/logout", null, { headers: headers })
        .map((response: Response) => {
          this.removeLoggedUser();
          return response.json();
        })
        .catch((error: Response): any => {
          return Observable.throw(error.json());
        });
    }
    else
      return Observable.throw("Not logged in.");
  }

  //For HTML *ngIf
  loggedIn(){
      if (localStorage.getItem("user"))
        return true;   
  }

  loggedUser() {
      if (localStorage.getItem("user"))
        return JSON.parse(localStorage.getItem("user"));   
  }

  removeLoggedUser(){
    if (localStorage.getItem("user"))
        localStorage.removeItem("user"); 
  }

  getInvitedUser(token: string): any {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.get(this.baseUrl + "/users/invited-user/" + token, { headers: headers })
      .map((response: Response) => {
        return response.json();
      })
      .catch((error: Response): any => {
        return Observable.throw(error.json());
      });
  }

  registerInvitedUser(user: User): Observable<string> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.put(this.baseUrl + "/users/finalize-invited-user/", { user: user }, { headers: headers })
      .map((response: Response) => {
        return response.json();
      })
      .catch((error: Response): any => {
        return Observable.throw(error.json());
      });
  }

  finalizeInvitationForExistingUser(email: string): Observable<string> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    return this.http.put(this.baseUrl + "/users/finalize-existing-invited-user/", { email: email }, { headers: headers })
      .map((response: Response) => {
        return response.json();
      })
      .catch((error: Response): any => {
        return Observable.throw(error.json());
      });
  }


  //Update header menu with user
  private loginUpdate = new Subject<any>();
  loginUpdateObs$ = this.loginUpdate.asObservable();
  public updateUserLogin(data: any) {
    this.loginUpdate.next(data);
  }
}
