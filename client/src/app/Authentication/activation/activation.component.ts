import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

//Services
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-activation',
  templateUrl: './activation.component.html',
  styleUrls: ['./activation.component.css']
})
export class ActivationComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router:Router, private authenticationService:AuthenticationService) { }

  message:string;

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      let activationCode = params["activationCode"];
      this.authenticationService.activate(activationCode).subscribe(
        status => {
          this.message = status + " Redirecting to login page...";
          setTimeout( () => {
            this.router.navigate(["/login"]);
          },2000);
          
        },
        error => {
          this.message = error;
        });
    });
  }

}
