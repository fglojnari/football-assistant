import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

//Classes
import { User } from './Users/User';

//Services
import { AuthenticationService } from './Authentication/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  user: User;

  //Events
  loginSub: Subscription;

  //Fires on first page load and page refresh
  ngOnInit() {

    //Checks if user is logged in
    this.authenticationService.authenticate().subscribe(
      logged => {
        if (logged)
          this.user = this.authenticationService.loggedUser();
      }, error => {
        this.user = null;
      });

    //Event that will 
    if (!this.user) {
      this.waitOnLogIn();
    }
  }

  showUserProfile() {
    this.router.navigate(['/profile']);
  }

  logout() {
    this.authenticationService.logout().subscribe(status => {
      this.user = null;
      this.waitOnLogIn();
    });
  }


  waitOnLogIn() {
    this.loginSub = this.authenticationService.loginUpdateObs$.subscribe(user => {
      this.user = user;
      this.loginSub.unsubscribe();
    });
  }

}
