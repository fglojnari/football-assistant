import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './Authentication/login/login.component';
import { RegisterComponent } from './Authentication/register/register.component';
import { ProfileComponent } from './Users/profile/profile.component';
import { ActivationComponent } from './Authentication/activation/activation.component';
import { AddTeamComponent } from './Teams/add-team/add-team.component';
import { HomeTeamComponent } from './Teams/home-team/home-team.component';
import { ViewTeamComponent } from './Teams/view-team/view-team.component';
import { RegisterInvitedComponent } from './Authentication/register-invited/register-invited.component';
import { PlaygroundsComponent } from './Playgrounds/playgrounds/playgrounds.component';
import { TournamentsComponent } from './Tournaments/tournaments/tournaments.component';
import { SingleTournamentComponent } from './Tournaments/single-tournament/single-tournament.component';



const appRoutes: Routes = [
    { path: '', component: HomeTeamComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'login', component: LoginComponent },
    { path: 'home', component: HomeTeamComponent },
    { path: 'view/:id', component: ViewTeamComponent },
    { path: 'profile', component: ProfileComponent },
    { path: 'activate/:activationCode', component: ActivationComponent },
    { path: 'add-team', component: AddTeamComponent },
    { path: 'invite-user/:token', component: RegisterInvitedComponent },
    { path: 'playgrounds', component: PlaygroundsComponent },
    { path: 'tournaments', component: TournamentsComponent},
    { path: 'tournaments/:id', component: SingleTournamentComponent},
    
];
export const Routing = RouterModule.forRoot(appRoutes);