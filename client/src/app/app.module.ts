import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

//Routing
import {Routing} from './app.routing';

//Components
import { AppComponent } from './app.component';
import { RegisterComponent } from './Authentication/register/register.component';
import { LoginComponent } from './Authentication/login/login.component';

//Services
import { AuthenticationService } from './Authentication/authentication.service';
import { UserService } from './Users/user.service';
import { PlaygroundService } from './Playgrounds/playground.service';
import { TeamsService } from './Teams/teams.service';
import { TournamentService } from './Tournaments/tournament.service';

//Directive
import { ProfileComponent } from './Users/profile/profile.component';
import { ActivationComponent } from './Authentication/activation/activation.component';
import { AddTeamComponent } from './Teams/add-team/add-team.component';

import { HomeTeamComponent } from './Teams/home-team/home-team.component';
import { ViewTeamComponent } from './Teams/view-team/view-team.component';
import { RegisterInvitedComponent } from './Authentication/register-invited/register-invited.component';
import { PlaygroundsComponent } from './Playgrounds/playgrounds/playgrounds.component';
import { TournamentsComponent } from './Tournaments/tournaments/tournaments.component';
import { SingleTournamentComponent } from './Tournaments/single-tournament/single-tournament.component';





@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    ProfileComponent,
    ActivationComponent,
    AddTeamComponent,
    HomeTeamComponent,
    ViewTeamComponent,
    RegisterInvitedComponent,
    PlaygroundsComponent,
    TournamentsComponent,
    SingleTournamentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    Routing
  ],
  providers: [AuthenticationService, UserService, TeamsService, PlaygroundService, TournamentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
