import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs';

//Classes
import { User } from './User';

@Injectable()
export class UserService {
  private baseUrl: string = "http://127.0.0.1:3000";

  constructor(private http: Http) { }

  getById(id: number): Observable<User> {
    return this.http.get(this.baseUrl + "/users/" + id)
      .map((response: Response) => {
        return response.json();
      })
      .catch((error: Response): any => {
        return Observable.throw(error.json());
      });
  }

  update(user: User): Observable<User> {
    let userLS = JSON.parse(localStorage.getItem("user"));
    if (userLS) {
      let headers = new Headers({ 'Content-Type': 'application/json', 'token': userLS.token });
      return this.http.put(this.baseUrl + "/users", JSON.stringify(user), { headers: headers })
        .map((response: Response) => {
          return response.json();
        })
        .catch((error: Response): any => {
          return Observable.throw(error.json());
        });
    }
  }

}
