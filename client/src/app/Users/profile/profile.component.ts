import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

//Classes
import { User } from '../User';

//Services
import { UserService } from '../user.service';
import { AuthenticationService } from '../../Authentication/authentication.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user: User;

  constructor(private userService: UserService,
    private router: Router, private authenticationService: AuthenticationService) { }

  ngOnInit() {

    //Checks if user is logged in and loads his past shopping carts
    this.authenticationService.authenticate().subscribe(
      logged => {
        this.user = this.authenticationService.loggedUser();
      }, error => {
        this.user = null;
        this.router.navigate(["/home"]);
      });
  }

  save(valid: boolean, formValue: NgForm) {
    if (valid && (formValue.form.value.password == formValue.form.value.confirmPassword)) {
      let data = formValue.form.value;
      this.userService.update(data).subscribe(user => {
        this.user.first_name = user.first_name;
        this.user.last_name = user.last_name;
        localStorage.setItem("user", JSON.stringify(this.user));
      });
    }
  }
}
