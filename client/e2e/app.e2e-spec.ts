import { ClientWebshopPage } from './app.po';

describe('client-webshop App', function() {
  let page: ClientWebshopPage;

  beforeEach(() => {
    page = new ClientWebshopPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
