var mongojs = require('mongojs');
var db = mongojs('mongodb://admin:admin@ds131510.mlab.com:31510/spletna_trgovina_test',['usersFootball']);
var _ = require('underscore');
let uuidV4 = require('uuid/v4');
let Mailgun = require('mailgun-js');
let apiKey = 'key-a017163afb865b713bae245407325515';
let domain = 'sandbox50d9d475f67b401f96c6454183f26ec6.mailgun.org';
let getDateInFuture = require('../helpers/dateFormatting').getDateInFuture;

var options = {
  server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } },
  replset: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } }
};

class AuthenticateBLL {
    static register(user, cb) {
        user.email_verification_data.expires = getDateInFuture(30);
        user.email_verification_data.token = uuidV4();
        user.email_verification_data.isVerified = false;
        db.usersFootball.save(user, function(err, id){
           if (id)
                AuthenticateBLL.sendEmail({email: user.email, token: user.email_verification_data.token}, function(err, result) {
                    if(!result) {
                        cb("Sending email failed!", null);
                    } else {
                        cb(null, "We send you a verification link to your email. Please check your email to complete registration!");
                    }
                });
            else {
                cb(err, null);
            }
        });
    }
    static login(username, password, cb) {
        db.usersFootball.findAndModify({
            query: {username: username, password: password, 'email_verification_data.isVerified': true },
            update: {$set: {'token_data.token': uuidV4(), 'token_data.expires': getDateInFuture(30) }},
            new: true
        }, (err, updatedUser) => {
            if(updatedUser) {
                let userForClient = _.omit(updatedUser, ['password', 'email_verification_data', 'token_data', '_id']);
                userForClient.token = updatedUser.token_data.token;
                cb(null, userForClient);
            } else {
                cb("Username or password is incorrect!");
            }
        });
    }
    static logout(user_id, cb) {
        db.usersFootball.findAndModify({
            query: {_id: mongojs.ObjectId(user_id)},
            update: {$set: {'token_data.expires': getDateInFuture(0)}},
            new: true
        }, (err, updatedUser) => {
            if(updatedUser) {
                cb(null, true);
            } else {
                cb("User is not logged in!", null);
            }
        });
    }
    static checkVerificationEmail(token, cb) {
        db.usersFootball.findAndModify({
            query: { 'email_verification_data.token': token, 
                     'email_verification_data.expires': { $gt: getDateInFuture(0) },
                     'email_verification_data.isVerified': false },
            update: { $set: { 'email_verification_data.isVerified': true } },
            new: true
        }, (err, updatedUser) => {
            if(updatedUser) {
                cb(null, "You have been sucessfully registered!");
            } else {
                cb("Your email verification date has expired or you already activated email!", null);
            }
        });
    }
    static authenticate(token, username, cb) {
        db.usersFootball.findOne({'token_data.token': token, 'username':username}, (err, user) => {
            if(user) {
                if (Date.now() < user.token_data.expires) {
                    cb(null, true);
                } else {
                    cb("Your session has expired!", null);
                }
            } else {
                cb("Your session has expired!", null);
            }
        });
    }
    static sendEmail(reqData, cb) {
        var mailgun = new Mailgun({apiKey: apiKey, domain: domain});

        var data = {
            from: 'footballapp@mb.si',
            to: reqData.email,
            subject: 'Fitness shop - confirm email',
            text: 'Please confirm your registration on following link: http://127.0.0.1:4200/activate/' + reqData.token
        };
        mailgun.messages().send(data, function (error, body) {
            if (error) {
                cb('Error while sending email!', null);
            }
            else {
                cb(null, body);
            }
        });
    }

    static registerInvitedInitial(email, cb) {
        db.usersFootball.save({ email: email }, (err, result) => {
            if (result) {
                cb(null, result);
            } else {
                cb("Internal server error!", null);
            }
        });
    }

    static registerInvitedFinalize(user_id, user_data, cb) {
        let updateObject = {
            first_name: user_data.first_name,
            last_name: user_data.last_name,
            username: user_data.username,
            password: user_data.password,
            role: 'user',
            token_data: {
                token: null,
                expires: null
            },
            email_verification_data: {
                token: null,
                expires: null,
                isVerified: true
            }
        };

        db.usersFootball.findAndModify({
            query: { _id: mongojs.ObjectId(user_id) },
            update: { $set: updateObject },
            new: true
        }, (err, updatedUser) => {
            if(updatedUser) {
                cb(null, "You have been sucessfully registered!");
            } else {
                cb("Registration failed!", null);
            }
        });
    }
}

module.exports = {
    AuthenticateBLL
};