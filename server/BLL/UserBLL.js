var mongojs = require('mongojs');
var db = mongojs('mongodb://admin:admin@ds131510.mlab.com:31510/spletna_trgovina_test',['usersFootbal']);
var _ = require('underscore');
let MemberBLL = require('../BLL/MemberBLL').MemberBLL;
let uuidV4 = require('uuid/v4');

class UserBLL {
    static get(id, cb) {
        db.usersFootball.find({ _id: mongojs.ObjectId(id) }, (err, result) => {
            if (result) {
                cb(null, result);
            } else {
                cb(err, null);
            }
        });
    }
    static getInvitedByToken(token, cb) {
        MemberBLL.findByToken(token, (err, user_id) => {
            db.usersFootball.findOne({ _id: mongojs.ObjectId(user_id) }, (err, user) => {
                if (user) {
                    cb(null, _.omit(user, '_id'));
                } else {
                    cb(err, null);
                }
            });
        });

    }
    static updateProfile(id, firstName, lastName, cb) {
        db.usersFootball.findAndModify({
            query: { _id: mongojs.ObjectId(id) },
            update: {$set: { first_name: firstName, last_name: lastName }},
            new: true
        }, (err, updatedUser) => {
            if(updatedUser) {
                let userForClient = _.omit(updatedUser, ['password', 'email_verification_data', 'token_data', '_id']);
                userForClient.token = updatedUser.token_data.token;
                cb(null, userForClient);
            } else {
                cb("Username or password is incorrect!");
            }
        });
    }
}

module.exports = {
    UserBLL
};