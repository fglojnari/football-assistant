var mongojs = require('mongojs');
var db = mongojs('mongodb://admin:admin@ds131510.mlab.com:31510/spletna_trgovina_test', ['playgrounds']);
var _ = require('underscore');

class PlaygroundBLL {
    static add(playground, cb) {
        db.playgrounds.save(playground, function (err, id) {
            if (id) {
                cb(null, "Playground added!");
            }
            else {
                cb("Internal server error!", null);
            }
        });
    }
    static getPlaygrounds(cb) {
        db.playgrounds.find(function (err, playgrounds) {
            if (playgrounds) {
                cb(null, playgrounds);
            }
            else {
                cb("Internal server error!", null);
            }
        });
    }

    static getTeamById(team_id, user_id, cb) {
        db.teams.findOne({admin_id: mongojs.ObjectId(user_id), _id: mongojs.ObjectId(team_id)}, function (err, team) {
            if (team) {
                cb(null, team);
            }
            else {
                cb("Internal server error!", null);
            }
        });
    }
    
    static inviteMember(team_id, emailOfInvitedUser, user_id, cb) {
        db.teams.findOne({admin_id: mongojs.ObjectId(user_id), _id: mongojs.ObjectId(team_id)}, function (err, team) {
            if (team) {
                MemberBLL.invite(team_id, emailOfInvitedUser, (err, result) => {
                    if (result) {
                        cb(null, "Invitation sent!");
                    } else {
                        cb("Invitation did not succeed!")
                    }
                });
            }
            else {
                cb("Internal server error!", null);
            }
        });
    }

}

module.exports = {
    PlaygroundBLL
};