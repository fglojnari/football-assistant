var mongojs = require('mongojs');
var db = mongojs('mongodb://admin:admin@ds131510.mlab.com:31510/spletna_trgovina_test', ['members', 'usersFootball', 'teams']);
let Member = require('../models/Member').Member;
let AuthenticateBLL = require('../BLL/AuthenticateBLL').AuthenticateBLL;

let Mailgun = require('mailgun-js');
let apiKey = 'key-a017163afb865b713bae245407325515';
let domain = 'sandbox50d9d475f67b401f96c6454183f26ec6.mailgun.org';
let getDateInFuture = require('../helpers/dateFormatting').getDateInFuture;
let uuidV4 = require('uuid/v4');

class MemberBLL {
    static invite(team_id, email, cb) {
        db.usersFootball.findOne({email: email}, (err, user) => {
            if(user) {
                let member = new Member(user._id, team_id, user.first_name, user.last_name, user.email);
                member.status = "invited";
                member.email_verification_data.token = uuidV4();
                member.email_verification_data.expires = getDateInFuture(1440);
                this.createMember(member, (err, result) => {
                    if (result) {
                        this.sendEmail({ email: email, token: member.email_verification_data.token }, (err, result) => {
                            if (result) {
                                cb(null, true);
                            } else {
                                cb("Email not sent", false);
                            }
                        });
                    }
                });
            } else {
                AuthenticateBLL.registerInvitedInitial(email, (err, user) => {
                    if (user._id) {
                        let member = new Member(user._id, team_id, "", "", email);
                        member.status = "invited";
                        member.email_verification_data.token = uuidV4();
                        member.email_verification_data.expires = getDateInFuture(1440);
                        this.createMember(member, (err, result) => {
                            if (result) {
                                this.sendEmail({email: email, token: member.email_verification_data.token }, (err, result) => {
                                    if (result) {
                                        cb(null, true);
                                    } else {
                                        cb("Email not sent", false);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
    static createMember(member, cb) {
        db.members.save(member, (err, result) => {
            if (result) {
                cb(null, true);
            }
        });
    }

    static findByToken(token, cb) {
        db.members.findOne({ 'email_verification_data.token': token }, (err, member) => {
            if (member) {
                if (Date.now() < member.email_verification_data.expires) {
                    cb(null, member.user_id);
                } else {
                    cb("Your session has expired!", null);
                }
            } else {
                cb("Member does not exist!", null);
            }

        });
    }

    static finalizeInvitation(user_data, cb) {
        let updateObject = { status: 'member' };
        if(user_data.first_name) {
            updateObject.first_name = user_data.first_name;
            updateObject.last_name = user_data.last_name;
        }
        db.members.findAndModify({
            query:  { email: user_data.email,
                     'email_verification_data.expires': { $gt: getDateInFuture(0) },
                     'status': 'invited' },
            update: { $set: updateObject },
            new: true
        }, (err, updatedUser) => {
            if(updatedUser) {
                AuthenticateBLL.registerInvitedFinalize(updatedUser.user_id, user_data, (err, result) => {
                    if(result) {
                        cb(null, "You have been sucessfully registered!");
                    } else {
                        cb("Internal server error!", null);
                    }
                });
            } else {
                cb("Your email verification date has expired or you already activated email!", null);
            }
        });
    }

    static finalizeExistingUser(email, cb) {
        db.members.findAndModify({
            query:  { email: email,
                     'email_verification_data.expires': { $gt: getDateInFuture(0) },
                     'status': 'invited' },
            update: { $set: { status: 'member'} },
            new: true
        }, (err, updatedUser) => {
            if(updatedUser) {
                cb(null, "Sucessfully added to team!");
            } else {
                cb("Your email verification date has expired or you already activated email!", null);
            }
        });
    }

    static getTeamMembers(team_id, user_id, cb) {
        db.teams.findOne({ _id: mongojs.ObjectId(team_id)}, (err, team) => {
            let teamAdminId = new String(team.admin_id);
            let userID = new String(user_id);
            if(teamAdminId.trim() == userID.trim()) {
                db.members.find({ team_id: team_id }, (err, members) => {
                    if (members) {
                        cb(null, members);
                    } else {
                        cb("Internal server error!", null);
                    }
                });
            } else {
                cb("Permission denied!", null);
            }
            
        });
    }

    static sendEmail(reqData, cb) {
        var mailgun = new Mailgun({apiKey: apiKey, domain: domain});

        var data = {
            from: 'footballapp@mb.si',
            to: reqData.email,
            subject: 'Football net - confirm email',
            text: 'You were invited to join team in football, please click on following link to join: http://127.0.0.1:4200/invite-user/' + reqData.token
        };
        mailgun.messages().send(data, function (error, body) {
            if (error) {
                cb('Error while sending email!', null);
            }
            else {
                cb(null, body);
            }
        });
    }
}

module.exports = {
    MemberBLL
};