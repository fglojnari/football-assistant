var mongojs = require('mongojs');
var db = mongojs('mongodb://admin:admin@ds131510.mlab.com:31510/spletna_trgovina_test',['tournaments']);
var _ = require('underscore');
let uuidV4 = require('uuid/v4');

class TournamentBLL {
    static add(tournamentData, cb) {
        db.tournaments.save(tournamentData, (err, id) => {
            if(id) {
                cb(null, "Turnament added!");
            } else {
                cb(err, null);
            }
        });
    }

    static get(cb) {
        db.tournaments.find((err, tournaments) => {
            if (tournaments) {
                cb(null, tournaments);
            } else {
                cb(err, null);
            }
        });
    }

    static getById(id, cb) {
        db.tournaments.findOne({ _id: mongojs.ObjectId(id) }, (err, tournament) => {
            if (tournament) {
                cb(null, tournament);
            } else {
                cb(err, null);
            }
        });
    }
}

module.exports = {
    TournamentBLL
};