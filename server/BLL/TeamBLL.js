var mongojs = require('mongojs');
var db = mongojs('mongodb://admin:admin@ds131510.mlab.com:31510/spletna_trgovina_test', ['teams', 'teamsTournaments']);
var _ = require('underscore');
let MemberBLL = require('../BLL/MemberBLL').MemberBLL;

class TeamBLL {
    static add(team, cb) {
        db.teams.save(team, function (err, id) {
            if (id) {
                cb(null, "Team added!");
            }
            else {
                cb("Internal server error!", null);
            }
        });
    }
    static getUsersTeams(user_id, cb) {
        db.teams.find({admin_id: mongojs.ObjectId(user_id)}, function (err, teams) {
            if (teams) {
                cb(null, teams);
            }
            else {
                cb("Internal server error!", null);
            }
        });
    }

    static getTeamById(team_id, user_id, cb) {
        db.teams.findOne({admin_id: mongojs.ObjectId(user_id), _id: mongojs.ObjectId(team_id)}, function (err, team) {
            if (team) {
                cb(null, team);
            }
            else {
                cb("Internal server error!", null);
            }
        });
    }
    
    static inviteMember(team_id, emailOfInvitedUser, user_id, cb) {
        db.teams.findOne({admin_id: mongojs.ObjectId(user_id), _id: mongojs.ObjectId(team_id)}, function (err, team) {
            if (team) {
                MemberBLL.invite(team_id, emailOfInvitedUser, (err, result) => {
                    if (result) {
                        cb(null, "Invitation sent!");
                    } else {
                        cb("Invitation did not succeed!")
                    }
                });
            }
            else {
                cb("Internal server error!", null);
            }
        });
    }
    static getTeamByCapitanId(user_id, cb) {
        db.teams.find({ admin_id: mongojs.ObjectId(user_id) }, (err, teams) => {
            if(teams) {
                cb(null, teams);
            } else {
                cb("Internal server error!", null);
            }
        });
    }

    static addTeamToTournament(team_id, tournament_id, cb) {
        db.teamsTournaments.save({ team_id: team_id, tournament_id: tournament_id }, (err, teamsTournaments) => {
            if(teamsTournaments) {
                cb(null, teamsTournaments);
            } else {
                cb("Internal server error!", null);
            }
        });
    }
    static getTeamsByTournamentId(tournament_id, cb) {
        console.log("evo turnir idja: ", tournament_id)
        db.teamsTournaments.find({ tournament_id: tournament_id }, (err, teamsTournament) => {
            console.log("err, teams: ", err, teamsTournament)
            if(teamsTournament) {
                var teamIds = teamsTournament.map(tT => {
                    return mongojs.ObjectId(tT.team_id);
                });
                console.log("mapirani teamIdjevi: ", teamIds);
                db.teams.find({_id: { $in: teamIds }}, (err, teams) => {
                    if(teams) {
                        cb(null, teams);
                    } else {
                        cb("Teams for this tournament does not exists!", null);
                    }
                });
            } else {
                cb("Internal server error!", null);
            }
        });
    }
}

module.exports = {
    TeamBLL
};