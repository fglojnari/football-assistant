let PlaygroundBLL = require('../BLL/PlaygroundBLL').PlaygroundBLL;
let Playground = require('../models/Playground').Playground;
var objectID = require('mongodb').ObjectID;

class PlaygroundController {
    static add(data, user_id, cb) {
        //validation of data
        let playground = new Playground(data.name, data.address);
        PlaygroundBLL.add(playground, (err, result) => {
            if(err) {
                cb(err, null);
            } else {
                cb(null, result);
            }
        });   
    }

    static getPlaygrounds(cb) {
        //validation of data
        PlaygroundBLL.getPlaygrounds((err, result) => {
            if(err) {
                cb(err, null);
            } else {
                cb(null, result);
            }
        });   
    }

    static getTeamById(team_id, user_id, cb) {
        //validation of data
        PlaygroundBLL.getTeamById(team_id, user_id, (err, result) => {
            if(err) {
                cb(err, null);
            } else {
                cb(null, result);
            }
        });   
    }
    static inviteMember(data, user_id, cb) {
        //validation of data
        PlaygroundBLL.inviteMember(data.team_id, data.email, user_id, (err, result) => {
            if(err) {
                cb(err, null);
            } else {
                cb(null, result);
            }
        });   
    }
    static getTeamMembers(team_id, user_id, cb) {
        //validation of data
        PlaygroundBLL.getTeamMembers(team_id, user_id, (err, result) => {
            if(err) {
                cb(err, null);
            } else {
                cb(null, result);
            }
        });   
    }

}

module.exports = {
    PlaygroundController
};