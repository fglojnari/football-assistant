let TeamBLL = require('../BLL/TeamBLL').TeamBLL;
let Team = require('../models/Team').Team;
var objectID = require('mongodb').ObjectID;
let TeamMemberBLL = require('../BLL/MemberBLL').MemberBLL;

class TeamController {
    static add(data, user_id, cb) {
        //validation of data
        let team = new Team(data.name, user_id);
        TeamBLL.add(team, (err, result) => {
            if(err) {
                cb(err, null);
            } else {
                cb(null, result);
            }
        });   
    }

    static getUsersTeams(user_id, cb) {
        //validation of data
        TeamBLL.getUsersTeams(user_id, (err, result) => {
            if(err) {
                cb(err, null);
            } else {
                cb(null, result);
            }
        });   
    }

    static getTeamById(team_id, user_id, cb) {
        //validation of data
        TeamBLL.getTeamById(team_id, user_id, (err, result) => {
            if(err) {
                cb(err, null);
            } else {
                cb(null, result);
            }
        });   
    }
    static inviteMember(data, user_id, cb) {
        //validation of data
        TeamBLL.inviteMember(data.team_id, data.email, user_id, (err, result) => {
            if(err) {
                cb(err, null);
            } else {
                cb(null, result);
            }
        });   
    }
    static getTeamMembers(team_id, user_id, cb) {
        //validation of data
        TeamMemberBLL.getTeamMembers(team_id, user_id, (err, result) => {
            if(err) {
                cb(err, null);
            } else {
                cb(null, result);
            }
        });   
    }

    static getTeamByCapitanId(user_id, cb) {
        TeamBLL.getTeamByCapitanId(user_id, (err, result) => {
            if(err) {
                cb(err, null);
            } else {
                cb(null, result);
            }
        });
    }
    static getTeamsByTournamentId(tournamentId, cb) {
        TeamBLL.getTeamsByTournamentId(tournamentId, (err, result) => {
            if(err) {
                cb(err, null);
            } else {
                cb(null, result);
            }
        });
    }
    static addTeamToTournament(data, cb) {
        TeamBLL.addTeamToTournament(data.team_id, data.tournament_id, (err, result) => {
            if(err) {
                cb(err, null);
            } else {
                cb(null, result);
            }
        });
    }

}

module.exports = {
    TeamController
};