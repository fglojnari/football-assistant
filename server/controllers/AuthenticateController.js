let AuthenticateBLL = require('../BLL/AuthenticateBLL').AuthenticateBLL;
let User = require('../models/User').User;

class AuthenticateController {
    static register(data, cb) {
        //validation of data
        let user = new User(data.first_name, data.last_name, data.username, data.password, data.email, "user");
        AuthenticateBLL.register(user, (err, result) => {
            if(err) {
                cb(err, null);
            } else {
                cb(null, result);

            }
        });
    }
    static login(data, cb) {
        AuthenticateBLL.login(data.username, data.password, (err, result) => {
            if(err) {
                cb(err, null);
            } else {
                cb(null, result);
            }
        });
    }
    static logout(user_id, cb) {
        AuthenticateBLL.logout(user_id, (err, result) => {
            if(err) {
                cb(err, null);
            } else {
                cb(null, result);
            }
        });
    }
    static checkVerificationEmail(token, cb) {
        AuthenticateBLL.checkVerificationEmail(token, function(err, result) {
            if(err) {
                cb(err, null);
            } else {
                cb(null, result);
            }
        });
    }
    static authenticate(token, username, cb) {
        AuthenticateBLL.authenticate(token, username, (err, result) => {
            if(err) {
                cb(err, null);
            } else {
                cb(null, result);
            }
        });
    }

}

module.exports = {
    AuthenticateController
};