let TournamentBLL = require('../BLL/TournamentBLL').TournamentBLL;

class TournamentConroller {
    static add(tournamentData, cb) {
        TournamentBLL.add(tournamentData, (err, result) => {
            if(result) {
                cb(null, result);
            } else {
                cb(err, null);
            }
        });
    }

    static get(cb) {
        TournamentBLL.get((err, result) => {
            if(err) {
                cb(err, null);
            } else {
                cb(null, result);
            }
        });
    }    

    static getById(id, cb) {
        TournamentBLL.getById(id, (err, result) => {
            if(err) {
                cb(err, null);
            } else {
                cb(null, result);
            }
        });
    }
}

module.exports = {
    TournamentConroller
};