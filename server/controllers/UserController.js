let UserBLL = require('../BLL/UserBLL').UserBLL;
let MemberBLL = require('../BLL/MemberBLL').MemberBLL;

class UserController {

    static getById(id, cb) {
        UserBLL.get(id, (err, result) => {
            if(err) {
                cb(err, null);
            } else {
                cb(null, result);
            }
        });
    }
    static getInvitedByToken(token, cb) {
        UserBLL.getInvitedByToken(token, (err, result) => {
            if(err) {
                cb(err, null);
            } else {
                cb(null, result);
            }
        });
    }
    
    static updateProfile(userId, userData, cb) {
        let firstNameValidated = userData.first_name && userData.first_name.length >= 2;
        let lastNameValidated = userData.last_name && userData.last_name.length >= 2;
        if(firstNameValidated && lastNameValidated) {
            UserBLL.updateProfile(userId, userData.first_name, userData.last_name, (err, result) => {
                if(result) {
                    cb(null, result);
                } else {
                    cb(err, null);
                }
            });
        } else {
            cb("Validation failed!", null);
        }
    }

    static finalizeRegistration(user_data, cb) {
        MemberBLL.finalizeInvitation(user_data, (err, result) => {
            if(result) {
                cb(null, result);
            } else {
                cb(err, null);
            }
        });
    }
    static finalizeInvitationExistingUser(email, cb) {
        MemberBLL.finalizeExistingUser(email, (err, result) => {
            if(result) {
                cb(null, result);
            } else {
                cb(err, null);
            }
        });
    }
    
}

module.exports = {
    UserController
};