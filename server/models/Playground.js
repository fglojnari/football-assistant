class Playground {
    constructor(name, address){
        this.name = name;
        this.address = address;
    }
}
module.exports = {
    Playground
};