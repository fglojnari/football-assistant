class Team {
    constructor(name, user_id){
        this.name = name;
        this.admin_id = user_id;
        this.members = [];
    }
}
module.exports = {
    Team
};