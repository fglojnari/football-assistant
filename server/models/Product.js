class Product {
    constructor(name, brand, price, description, keywords, category, image_url){
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.description = description;
        this.keywords = keywords;
        this.category = category;
        this.date_added = null;
        this.image_url = image_url;
        this.ratings = [];
    }
}
module.exports = {
    Product
};