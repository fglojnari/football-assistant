class Comment {
    constructor(text, user_id, product_id, username) {
        this.text = text;
        this.user_id = user_id;
        this.username = username;
        this.product_id = product_id;
    }
}
module.exports = {
    Comment
};