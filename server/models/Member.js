class Member {
    constructor(user_id, team_id, first_name, last_name, email) {
        this.user_id = user_id;
        this.team_id = team_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.status = null;
        this.email_verification_data = {
            token: null,
            expires: null
        }
    }
}
module.exports = {
    Member
};