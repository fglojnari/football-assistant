class User {
    constructor(first_name, last_name, username, password, email, role, token) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.username = username;
        this.password = password;
        this.email = email;
        this.role = role;
        this.token_data = {
            token: null,
            expires: null
        };
        this.email_verification_data = {
            token: null,
            expires: null,
            isVerified: false
        };
    }
}
module.exports = {
    User
};