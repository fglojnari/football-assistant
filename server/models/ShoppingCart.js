class ShoppingCart {
    constructor(user_id, product_ids, buy_date) {
        this.user_id = user_id;
        this.product_ids = product_ids;
        this.buy_date = buy_date;
    }
}
module.exports = {
    ShoppingCart
};