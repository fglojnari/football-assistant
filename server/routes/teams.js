var express = require('express');
var router = express.Router();
let TeamController = require('../controllers/TeamController').TeamController;
let AuthenticateBLL = require('../BLL/AuthenticateBLL').AuthenticateBLL;
var checkPermisions = require('../helpers/permissions').checkPermisions;

// Middleware route. Everything that is in this router.use function, is done on every "product" route. Here
//we will check our token that comes from client.

router.post('/', function (req, res, next) {
    checkPermisions(req.headers.token, 'TeamController.add', (err, user_id) => {
        if (user_id) {
            TeamController.add(req.body, user_id, (err, result) => {
                if (err) {
                    res.status(404).json(err);
                } else {
                    res.status(200).json(result);
                }
            });
        } else {
            res.status(404).json(err);
        }
    });
});

router.get('/', function (req, res, next) {
    checkPermisions(req.headers.token, 'TeamController.getUsersTeams', (err, user_id) => {
        if (user_id) {
            TeamController.getUsersTeams(user_id, (err, result) => {
                if (err) {
                    res.status(404).json(err);
                } else {
                    res.status(200).json(result);
                }
            });
        } else {
            res.status(404).json(err);
        }
    });
});
router.get('/:id', function (req, res, next) {
    checkPermisions(req.headers.token, 'TeamController.getTeamById', (err, user_id) => {
        if (user_id) {
            TeamController.getTeamById(req.params.id, user_id, (err, result) => {
                if (err) {
                    res.status(404).json(err);
                } else {
                    res.status(200).json(result);
                }
            });
        } else {
            res.status(404).json(err);
        }
    });
});

router.get('/of/user', function (req, res, next) {
    checkPermisions(req.headers.token, 'TeamController.getTeamByCapitanId', (err, user_id) => {
        if (user_id) {
            TeamController.getTeamByCapitanId(user_id, (err, result) => {
                if (err) {
                    res.status(404).json(err);
                } else {
                    res.status(200).json(result);
                }
            });
        } else {
            res.status(404).json(err);
        }
    });
});
router.get('/tournament/:tournament_id', function (req, res, next) {
    console.log("u ruti za timove turnira")
    checkPermisions(req.headers.token, 'TeamController.getTeamsByTournamentId', (err, user_id) => {
        if (user_id) {
            TeamController.getTeamsByTournamentId(req.params.tournament_id, (err, result) => {
                if (err) {
                    res.status(404).json(err);
                } else {
                    res.status(200).json(result);
                }
            });
        } else {
            res.status(404).json(err);
        }
    });
});
router.post('/tournament', function (req, res, next) {
    checkPermisions(req.headers.token, 'TeamController.addTeamToTournament', (err, user_id) => {
        if (user_id) {
            TeamController.addTeamToTournament(req.body, (err, result) => {
                if (err) {
                    res.status(404).json(err);
                } else {
                    res.status(200).json(result);
                }
            });
        } else {
            res.status(404).json(err);
        }
    });
});
router.post('/invite', function (req, res, next) {
    checkPermisions(req.headers.token, 'TeamController.inviteMember', (err, user_id) => {
        if (user_id) {
            TeamController.inviteMember(req.body, user_id, (err, result) => {
                if (err) {
                    res.status(404).json(err);
                } else {
                    res.status(200).json(result);
                }
            });
        } else {
            res.status(404).json(err);
        }
    });
});

router.get('/members/:team_id', function (req, res, next) {
    checkPermisions(req.headers.token, 'TeamController.getTeamMembers', (err, user_id) => {
        if (user_id) {
            TeamController.getTeamMembers(req.params.team_id, user_id, (err, result) => {
                if (err) {
                    res.status(404).json(err);
                } else {
                    res.status(200).json(result);
                }
            });
        } else {
            res.status(404).json(err);
        }
    });
});
module.exports = router;