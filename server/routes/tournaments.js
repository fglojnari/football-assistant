var express = require('express');
var router = express.Router();
let TournamentsController = require('../controllers/TournamentsController').TournamentConroller;
var checkPermisions = require('../helpers/permissions').checkPermisions;

router.get('/', (req, res, next) => {
    checkPermisions(req.headers.token, 'TournamentsController.get', (err, user_id) => {
        if (user_id) {
            TournamentsController.get((err, result) => {
                if(err) {
                    res.status(404).json(err);
                } else {
                    res.status(200).json(result);
                }
            });
        } else {
            res.status(404).json(err);
        }
    });
});

router.get('/:id', (req, res, next) => {
    checkPermisions(req.headers.token, 'TournamentsController.getById', (err, user_id) => {
        if (user_id) {
            TournamentsController.getById(req.params.id, (err, result) => {
                if(err) {
                    res.status(404).json(err);
                } else {
                    res.status(200).json(result);
                }
            });
        } else {
            res.status(404).json(err);
        }
    });
});

router.post('/', (req, res, next) => {
    console.log("evo nas u ruteru za post: ", req.body)
    checkPermisions(req.headers.token, 'TournamentsController.add', (err, user_id) => {
        if (user_id) {
            TournamentsController.add(req.body, (err, result) => {
                if(err) {
                    res.status(404).json(err);
                } else {
                    res.status(200).json(result);
                }
            });
        } else {
            res.status(404).json(err);
        }
    });
});
module.exports = router;
