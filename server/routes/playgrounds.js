var express = require('express');
var router = express.Router();
let PlaygroundController = require('../controllers/PlaygroundController').PlaygroundController;
let AuthenticateBLL = require('../BLL/AuthenticateBLL').AuthenticateBLL;
var checkPermisions = require('../helpers/permissions').checkPermisions;

// Middleware route. Everything that is in this router.use function, is done on every "product" route. Here
//we will check our token that comes from client.

router.post('/', function (req, res, next) {
    checkPermisions(req.headers.token, 'PlaygroundController.add', (err, user_id) => {
        if (user_id) {
            PlaygroundController.add(req.body, user_id, (err, result) => {
                if (err) {
                    res.status(404).json(err);
                } else {
                    res.status(200).json(result);
                }
            });
        } else {
            res.status(404).json(err);
        }
    });
});

router.get('/', function (req, res, next) {
    checkPermisions(req.headers.token, 'PlaygroundController.getPlaygrounds', (err, user_id) => {
        if (user_id) {
            PlaygroundController.getPlaygrounds((err, result) => {
                if (err) {
                    res.status(404).json(err);
                } else {
                    res.status(200).json(result);
                }
            });
        } else {
            res.status(404).json(err);
        }
    });
});
router.get('/:id', function (req, res, next) {
    checkPermisions(req.headers.token, 'PlaygroundController.getTeamById', (err, user_id) => {
        if (user_id) {
            PlaygroundController.getTeamById(req.params.id, user_id, (err, result) => {
                if (err) {
                    res.status(404).json(err);
                } else {
                    res.status(200).json(result);
                }
            });
        } else {
            res.status(404).json(err);
        }
    });
});
module.exports = router;