var express = require('express');
var router = express.Router();
let UserController = require('../controllers/UserController').UserController;
var checkPermisions = require('../helpers/permissions').checkPermisions;

router.get('/:id', (req, res, next) => {
  UserController.getById(req.params.id, (err, result) => {
    if(err) {
        res.status(404).json(err);
    } else {
        res.status(200).json(result);
    }
  })
});
router.put('/', (req, res, next) => {
    checkPermisions(req.headers.token, 'UserController.updateProfile', (err, user_id) => {
        if (user_id) {
            UserController.updateProfile(user_id, req.body, (err, updatedUser) => {
                if(err)
                    res.status(404).json(err);
                else
                    res.status(200).json(updatedUser);
            });
        } else {
            res.status(404).json(err);
        }
    });
});
router.get('/invited-user/:token', (req, res, next) => {
  UserController.getInvitedByToken(req.params.token, (err, result) => {
    if(err) {
        res.status(404).json(err);
    } else {
        res.status(200).json(result);
    }
  })
});

router.put('/finalize-invited-user', (req, res, next) => {
  UserController.finalizeRegistration(req.body.user, function(err, result) {
      console.log(err, result)
    if(!result) {
      res.status(401).json(err);
    } else {
      res.status(200).json(result);
    }
  });
});

router.put('/finalize-existing-invited-user', (req, res, next) => {
  UserController.finalizeInvitationExistingUser(req.body.email, function(err, result) {
      console.log(err, result)
    if(!result) {
      res.status(401).json(err);
    } else {
      res.status(200).json(result);
    }
  });
});

module.exports = router;
