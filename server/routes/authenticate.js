var express = require('express');
var router = express.Router();
let AuthenticateController = require('../controllers/AuthenticateController').AuthenticateController;
var checkPermisions = require('../helpers/permissions').checkPermisions;

router.post('/registration', (req, res, next) => {
  let data = req.body;
  AuthenticateController.register(data, (err, result) => {
    if(result) {
        res.status(200).json(result);
    } else {
        res.status(404).json(err);
    }
  })
});

router.post('/login', (req, res, next) => {
  AuthenticateController.login(req.body, (err, result) => {
    if(result) {
        res.status(200).json(result);
    } else {
      res.status(404).json(err);
    }
  })
});

router.post('/logout', (req, res, next) => {
    checkPermisions(req.headers.token, 'AuthenticateController.logout', (err, user_id) => {
        if (user_id) {
          AuthenticateController.logout(user_id, (err, result) => {
            if(err) {
                res.status(401).json(err);
            } else {
                res.status(200).json(result);
            }
          });
        } else {
            res.status(404).json(err);
        }
    });
});

router.get('/token/:token', (req, res, next) => {
  AuthenticateController.checkVerificationEmail (req.params.token, function(err, result) {
    if(!result) {
      res.status(401).json(err);
    } else {
      res.status(200).json(result);
    }
  });
});

router.get('/loggedin', (req, res, next) => {
  AuthenticateController.authenticate (req.headers.token, req.headers.username, function(err, result) {
    if(!result) {
      res.status(401).json(err);
    } else {
      res.status(200).json(result);
    }
  });
});


module.exports = router;