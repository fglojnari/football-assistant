var  getDateInFuture = function(minsInFuture) {
    let today = new Date();
    let min = today.getMinutes() + minsInFuture;
    let hh = today.getHours();
    let dd = today.getDate();
    let mm = today.getMonth();
    let yyyy = today.getFullYear();
    let newDate = new Date(yyyy, mm, dd, hh, min); 
    return newDate;
}

module.exports = {
    getDateInFuture
}