//In this file we will keep our permissions in key-value form for all methods that we have.
//key is the method in controller and value is array of roles that can access to that method

var mongojs = require('mongojs');
var db = mongojs('mongodb://admin:admin@ds131510.mlab.com:31510/spletna_trgovina_test',['users']);
var _ = require('underscore');
let getDateInFuture = require('../helpers/dateFormatting').getDateInFuture;

var permissions = {
    'AuthenticateController.logout': ['admin', 'user'],

    'UserController.updateProfile': ['admin', 'user'],


    //Football
    'TeamController.add': ['user'],
    'TeamController.getUsersTeams': ['user'],
    'TeamController.getTeamById': ['user'],
    'TeamController.inviteMember': ['user'],
    'TeamController.getTeamMembers': ['user'],
    'TeamController.getTeamByCapitanId': ['user'],
    'TeamController.getTeamsByTournamentId': ['user'],
    'TeamController.addTeamToTournament': ['user'],

    'PlaygroundController.add': ['user'],
    'PlaygroundController.getPlaygrounds': ['user'],

    'TournamentsController.getById': ['user'],
    'TournamentsController.get': ['user'],
    'TournamentsController.add': ['user'],
};

var checkPermisions = function(token, methodName, cb) {
    if(token) {
        var user  = db.usersFootball.findOne({ 'token_data.token': token }, (err, user) => {
            if(!user) {
                cb("Permission denied!", null);
            } else {
                if (_.contains(permissions[methodName], user.role) && (getDateInFuture(0) < user.token_data.expires)) {
                    cb(null, user._id);
                } else {
                    cb("Permission denied!", null);
                }
            }
        });
    } else {
        cb("Permission denied!", null);
    }
};

module.exports = {
    checkPermisions
}